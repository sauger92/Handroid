package threadslifecycle.android.fr.handroid;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Camera;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.lang.Integer.parseInt;

/**
 * Created by jerem on 31/03/2018.
 */

public class GameLive extends AppCompatActivity {

    private static final int CAMERA_REQUEST = 1888;
    static final int REQUEST_IMAGE_CAPTURE = 1;

    String photo = " ";
    private int score1 = 0;
    private int score2 = 0;
    private int tir1 = 0;
    private int tir2 = 0;
    private Button But1;
    private Button But2;
    private Button Tir1;
    private Button Tir2;
    private Button finMatch;
    public TextView TirRate1;
    public TextView TirRate2;

    //var pour la photo
    private Camera camera;
    private SurfaceView surfaceCamera;
    private Boolean isPreview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gamelive);


        But1 = (Button) findViewById(R.id.But1);
        But2 = (Button) findViewById(R.id.But2);
        Tir1 = (Button) findViewById(R.id.Tir1);
        Tir2 = (Button) findViewById(R.id.Tir2);
        TirRate1 = (TextView) findViewById(R.id.tirRate1);
        TirRate2 = (TextView) findViewById(R.id.tirRate2);

        //pour le score
        final TextView Score1 = (TextView) findViewById(R.id.score1);
        final TextView Score2 = (TextView) findViewById(R.id.score2);


        //pour le nom des équipes
        final TextView Nom1 = (TextView) findViewById(R.id.Nom1);
        final TextView Nom2 = (TextView) findViewById(R.id.Nom2);

        finMatch = (Button) findViewById(R.id.finMatch);
        finMatch.setText("Fin du Match");

        But1.setText("BUT");
        But2.setText("BUT");
        Tir1.setText("Tir raté");
        Tir2.setText("Tir raté");

        Score1.setText("0");
        Score2.setText("0");
        TirRate1.setText("0");
        TirRate2.setText("0");

        final GameDataSource datasource = new GameDataSource(this);
        datasource.open();


        Intent intent = getIntent();
        final Game game = (Game) intent.getSerializableExtra("newGame");

        //affichage des noms d'équipe
        Nom1.setText(game.nom_equipe);
        Nom2.setText(game.adversaire);

        //envoi des scores à la bdd
            But1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    //+1 à l'affichage
                    score1 = Integer.parseInt(Score1.getText().toString()) + 1;
                    Score1.setText(String.valueOf(score1));
                    tir1 = Integer.parseInt(TirRate1.getText().toString()) + 1;
                    TirRate1.setText(String.valueOf(tir1));
                }
            });

        But2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //+1 à l'affichage
                score2 = Integer.parseInt(Score2.getText().toString()) + 1;
                Score2.setText(String.valueOf(score2));
                tir2 = Integer.parseInt(TirRate2.getText().toString()) + 1;
                TirRate2.setText(String.valueOf(tir2));
            }
        });
        Tir1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //+1 à l'affichage
                tir1 = Integer.parseInt(TirRate1.getText().toString()) + 1;
                TirRate1.setText(String.valueOf(tir1));
            }
        });

        Tir2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //+1 à l'affichage
                tir2 = Integer.parseInt(TirRate2.getText().toString()) + 1;
                TirRate2.setText(String.valueOf(tir2));
            }
        });

        //envoi des scores à la bdd
        finMatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                game.tir_equipe = tir1;
                game.tir_adverse = tir2;
                game.score_equipe = score1;
                game.score_adverse = score2;
                game.photo = photo;
                AddMovieAsyncTask db = new AddMovieAsyncTask(game);
                db.execute();
                datasource.createGame(game);
                Intent intent = new Intent(GameLive.this, Stats.class);  //Lancer l'activité Stats
                startActivity(intent);

            }
        });



    }

        //pour avoir l'icone de l'appareil photo dans le menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.pic, menu);
        return true;

    }

        //dès qu'on clique sur la photo
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.picitem:
                dispatchTakePictureIntent();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }





    static final int REQUEST_TAKE_PHOTO = 1;

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this, "fr.android.threadslifecycle.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);

                photo = photoURI.toString();

            }
        }
    }
    String mCurrentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }





}


