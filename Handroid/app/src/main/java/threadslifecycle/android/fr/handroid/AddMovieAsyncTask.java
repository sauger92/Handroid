package threadslifecycle.android.fr.handroid;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AddMovieAsyncTask extends AsyncTask<String, String, String> {
Game g;
Context c;




    public AddMovieAsyncTask(Game game) {
    this.g =game;

        //Display proggress bar

    }

    @Override
    protected String doInBackground(String... params) {
        HttpJsonParser httpJsonParser = new HttpJsonParser();
        Map<String, String> httpParams = new HashMap<>();
        //Populating request parameters

        httpParams.put("adversaire", this.g.adversaire);
        httpParams.put("score_equipe", String.valueOf(this.g.score_equipe));
        httpParams.put("score_adverse", String.valueOf(this.g.score_adverse));
        httpParams.put("latitude", String.valueOf(this.g.latitude));
        httpParams.put("longitude", String.valueOf(this.g.longitude));
        httpParams.put("geocoding", this.g.geocoding);
        httpParams.put("nb_photo", String.valueOf(this.g.nb_photo));
        httpParams.put("tir_equipe", String.valueOf(this.g.tir_equipe));
        httpParams.put("tir_adverse", String.valueOf(this.g.tir_adverse));
        httpParams.put("nom_equipe", this.g.nom_equipe);
        httpParams.put("photo", this.g.photo);

        JSONObject jsonObject = httpJsonParser.makeHttpRequest(
                "https://floatboat.000webhostapp.com/PPE/" + "add_game.php", "POST", httpParams);
        try {
            int success = jsonObject.getInt("success");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


}
