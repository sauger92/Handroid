package threadslifecycle.android.fr.handroid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Demarrage extends AppCompatActivity {
    public static final String PREFNAME1 = "nomEquipe";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demarrage);

        final EditText nomEquipe = (EditText) findViewById(R.id.name);
        Button inscription = (Button) findViewById(R.id.insciption);

        final SharedPreferences settings1 = getSharedPreferences(PREFNAME1, 0);
        settings1.getString("key1", "0");

        inscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //sauvegarder le nom d'equipe dans un fichier
                String name  = nomEquipe.getText().toString();
                SharedPreferences.Editor editor = settings1.edit();

                editor.putString(PREFNAME1, name);
                editor.commit();

                    //puis dirigier vers une autre page
                    Intent intent = new Intent(Demarrage.this, Stats.class);
                    startActivity(intent);


            }
        });
    }
}
