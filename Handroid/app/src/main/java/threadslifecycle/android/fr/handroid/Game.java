package threadslifecycle.android.fr.handroid;


import java.io.Serializable;

public class Game implements Serializable {
    public long id;
    public String adversaire;
    public int score_equipe;
    public int score_adverse;
    public double latitude;
    public double longitude;
    public String geocoding;
    public int nb_photo;
    public int tir_equipe;
    public int tir_adverse;
    public String nom_equipe;
    public String photo;
    Game()
    {

        adversaire = " ";
        score_equipe = 0;
        score_adverse = 0;
        latitude= 0;
        longitude = 0;
        String geocoding = " ";
        int nb_photo =0;
        tir_equipe = 0;
        tir_adverse = 0;
        nom_equipe = " ";
        photo = " ";

    }
}

