package threadslifecycle.android.fr.handroid;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

public class DBManager extends AsyncTask<String, Void, Game[]> {
    InputStream is = null;
    String result = "";
    String link = "https://handroid.000webhostapp.com/Handroid/connexion.php";
    int[] yData;
    PieChart graphe;
    String[] xData = {"Victoires","Nul","Défaites"};
    public Context context;
    public ListeMatches listMatch;

    public DBManager(Context context, ListeMatches lm) {
        this.context = context;
        this.listMatch = lm;

    }

    @Override
    protected Game[] doInBackground(String... strings) {
        try {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(link);

            //httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
        } catch (Exception e) {
            Log.e("log_tag", "Error in http connection " + e.toString());
        }
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");

            }
            is.close();
            result = sb.toString();

        } catch (Exception e) {
            Log.e("log_tag", "Error converting result " + e.toString());
        }
        // Parse les données JSON
        String returnString = null;
        try {
            JSONArray jArray = new JSONArray(result);
            String[] lattitude;
            String[] longitude;
            Game[] game = new Game[jArray.length()];
            System.out.println(jArray.length());
            for (int i = 0; i < jArray.length(); i++) {
                JSONObject json_data = jArray.getJSONObject(i);

                game[i] = new Game();
                game[i].id = json_data.getInt("id");
                game[i].adversaire = json_data.getString("adversaire");
                game[i].score_adverse = json_data.getInt("score_adverse");
                game[i].score_equipe = json_data.getInt("score_equipe");
                game[i].latitude = json_data.getDouble("latitude");
                game[i].longitude = json_data.getDouble("longitude");
                game[i].geocoding = json_data.getString("geocoding");
                game[i].nb_photo = json_data.getInt("nb_photo");
                game[i].adversaire = json_data.getString("adversaire");
                game[i].tir_adverse = json_data.getInt("tir_adverse");
                game[i].tir_equipe = json_data.getInt("tir_equipe");
                game[i].nom_equipe = json_data.getString("nom_equipe");
                game[i].photo = json_data.getString("photo");



                returnString += "\n\t" + jArray.getJSONObject(i);


            }
            System.out.println("coucou2" + returnString);
            return game;

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return new Game[0];
    }

    @Override
    protected void onPostExecute(final Game[] values ){
        super.onPostExecute(values);
        int taille = 5;
        int victoire =0;
        int nul = 0;
        int stop = 0;
        int defaite = 0;

        for (int i = values.length-1; i>=0 ; i--) {
            TableRow table = new TableRow(this.context);

            if (values[i].score_equipe > values[i].score_adverse) victoire++;
            if (values[i].score_equipe < values[i].score_adverse) defaite++;
            if (values[i].score_equipe == values[i].score_adverse) nul++;

            TextView equipe = new TextView(this.context);
            equipe.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            equipe.setText(values[i].nom_equipe + " ");
            table.addView(equipe);
            TextView scoreEquipe = new TextView(this.context);
            scoreEquipe.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
            scoreEquipe.setText(Integer.toString(values[i].score_equipe) + " ");
            table.addView(scoreEquipe);
            TextView scoreAdversaire = new TextView(this.context);
            scoreAdversaire.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
            scoreAdversaire.setText(Integer.toString(values[i].score_adverse) + " ");
            table.addView(scoreAdversaire);
            TextView adversaire = new TextView(this.context);
            adversaire.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
            adversaire.setText(values[i].adversaire);
            table.addView(adversaire);
            final int finalI = i;
            table.setOnClickListener(new View.OnClickListener() {


                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, DetailMatch.class);  //Lancer l'activité Stats
                    intent.putExtra("DetailGame", (Serializable) values[finalI]);
                    context.startActivity(intent);    //Afficher la vue
                }
            });

            TableLayout t = ((Activity)context).findViewById(R.id.tableauScore);
            System.out.println("coucou3 "+ t);
            t.addView(table);
            yData = new int[]{victoire, nul, defaite};
            graphe = (PieChart) ((Activity)context).findViewById(R.id.IDgraphe);
            addDataSet();

        }
    }
    private void addDataSet(){
        //le conseil du jour : utiliser des log.d pour checker

        ArrayList<PieEntry> yEntrys = new ArrayList<>();
        ArrayList<String> xEntrys = new ArrayList<>();

        for(int i = 0; i < yData.length ; i++)
        {
            yEntrys.add(new PieEntry(yData[i], i));
        }

        for(int i = 1; i < xData.length ; i++)
        {
            xEntrys.add(xData[i]);
        }


        PieDataSet pieDataSet = new PieDataSet(yEntrys,"Victoire / Nul / Défaite");
        pieDataSet.setSliceSpace(3); //espace entre les datas
        pieDataSet.setValueTextSize(12); //taille des datas

        ArrayList<Integer> couleurs = new ArrayList<>();
        couleurs.add(Color.GREEN);
        couleurs.add(Color.GRAY);
        couleurs.add(Color.RED);

        pieDataSet.setColors(couleurs);


        Legend legende = graphe.getLegend();
        legende.setForm(Legend.LegendForm.CIRCLE);
        //legende.setPosition(Legend.LegendPosition.LEFT_OF_CHART);


        //on set les data au graphe
        PieData pieData =  new PieData(pieDataSet);
        graphe.setData(pieData);
        graphe.setNoDataText("0 match joué pour le moment");
        graphe.invalidate();
    }
}
