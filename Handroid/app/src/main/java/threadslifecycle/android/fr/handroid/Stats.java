package threadslifecycle.android.fr.handroid;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;

import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import org.w3c.dom.Text;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Stats extends AppCompatActivity {
    int[] yData;
    PieChart graphe;
    String[] xData = {"Victoires","Nul","Défaites"};
    TextView vic;
    TextView mnul;
    TextView def;

    public GameDataSource datasource;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stats);

        datasource = new GameDataSource(this);
        datasource.open();


int taille = 5;
int victoire =0;
int nul = 0;
int stop = 0;
int defaite = 0;
        final List<Game> values =  datasource.getAllComments();
        if (values.size()-5>0)stop = values.size()-5;

        for(int i=values.size()-1; i>= stop; i--)
        {
            TableRow table = new TableRow(this);

            TableRow.LayoutParams param = new TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT);
            param.weight=2;
            param.gravity= Gravity.CENTER_VERTICAL;

            table.setLayoutParams(param);

            if(values.get(i).score_equipe>values.get(i).score_adverse)victoire++;
            if(values.get(i).score_equipe<values.get(i).score_adverse)defaite++;
            if(values.get(i).score_equipe==values.get(i).score_adverse)nul++;


            TextView equipe = new TextView(this);
            equipe.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
            equipe.setText(values.get(i).nom_equipe+" ");
            table.addView(equipe);

            TextView scoreEquipe = new TextView(this);
            scoreEquipe.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
            scoreEquipe.setText(Integer.toString(values.get(i).score_equipe)+" ");
            table.addView(scoreEquipe);

            TextView scoreAdversaire = new TextView(this);
            scoreAdversaire.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
            scoreAdversaire.setText(Integer.toString(values.get(i).score_adverse)+" ");
            table.addView(scoreAdversaire);

            TextView adversaire = new TextView(this);
            adversaire.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
            adversaire.setText(values.get(i).adversaire);
            table.addView(adversaire);


            final int finalI = i;
            table.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Stats.this, DetailMatch.class);  //Lancer l'activité Stats
                    intent.putExtra("DetailGame", (Serializable) values.get(finalI));
                    startActivity(intent);    //Afficher la vue
                }
            });

            TableLayout t = (TableLayout) findViewById(R.id.tableauScore);
            t.addView(table);
        }

        vic = (TextView) findViewById(R.id.nbreVic);
        if (victoire<=1)vic.setText(victoire+" Victoire");
        if (victoire>=2)vic.setText(victoire+" Victoires");

        mnul = (TextView) findViewById(R.id.nbreNul);
        if (nul<=1)mnul.setText(nul+" Match Nul");
        if (nul>=2)mnul.setText(nul+" Matches Nuls");

        def = (TextView) findViewById(R.id.nbreDef);
        if (defaite<=1)def.setText(defaite+" Défaite");
        if (defaite>=12)def.setText(defaite+" Défaites");

        yData = new int[]{victoire, nul, defaite};
        graphe = (PieChart) findViewById(R.id.IDgraphe);
        datasource.close();
        addDataSet();

    }
    private void addDataSet(){
        //le conseil du jour : utiliser des log.d pour checker

        ArrayList<PieEntry> yEntrys = new ArrayList<>();
        ArrayList<String> xEntrys = new ArrayList<>();

        for(int i = 0; i < yData.length ; i++)
        {
            yEntrys.add(new PieEntry(yData[i], i));
        }

        for(int i = 1; i < xData.length ; i++)
        {
            xEntrys.add(xData[i]);
        }


        PieDataSet pieDataSet = new PieDataSet(yEntrys,"Victoire / Nul / Défaite");
        pieDataSet.setSliceSpace(3); //espace entre les datas
        pieDataSet.setValueTextSize(12); //taille des datas

        ArrayList<Integer> couleurs = new ArrayList<>();
        couleurs.add(Color.GREEN);
        couleurs.add(Color.GRAY);
        couleurs.add(Color.RED);

        pieDataSet.setColors(couleurs);

        pieDataSet.setDrawValues(false);
        //Legend legende = graphe.getLegend();
        //legende.setForm(Legend.LegendForm.CIRCLE);
        //legende.setPosition(Legend.LegendPosition.LEFT_OF_CHART);

        //graphe.setDrawHoleEnabled(false); //pour enlever le trou du milieu

        graphe.getDescription().setEnabled(false);
        //on set les data au graphe
        PieData pieData =  new PieData(pieDataSet);
        graphe.setData(pieData);
        //graphe.setNoDataText("0 match joué pour le moment");
        graphe.invalidate();

    }
}