package threadslifecycle.android.fr.handroid;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteHelper extends SQLiteOpenHelper  {
    public static final String TABLE_GAME = "game";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_ADVERSAIRE = "adversaire";
    public static final String COLUMN_SCORE_EQUIPE = "score_equipe";
    public static final String COLUMN_SCORE_ADVERSE = "score_adverse";
    public static final String COLUMN_LATITUDE = "latitude";
    public static final String COLUMN_LONGITUDE = "longitude";
    public static final String COLUMN_GEOCODING = "geocoding";
    public static final String COLUMN_NB_PHOTO = "nb_photo";
    public static final String COLUMN_TIR_EQUIPE = "tir_equipe";
    public static final String COLUMN_TIR_ADVERSE = "tir_adverse";
    public static final String COLUMN_NOM_EQUIPE = "nom_equipe";
    public static final String COLUMN_PHOTO = "photo";


    private static final String DATABASE_NAME = "game.db";
    private static final int DATABASE_VERSION = 1;

    // Commande sql pour la création de la base de données
    private static final String DATABASE_CREATE = "create table "
            + TABLE_GAME + "(" + COLUMN_ID
            + " integer primary key autoincrement, " + COLUMN_ADVERSAIRE
            + " text not null, "+ COLUMN_SCORE_EQUIPE+" integer, "+COLUMN_SCORE_ADVERSE+" integer, "+COLUMN_LATITUDE+" double, " +
             COLUMN_LONGITUDE+ " double, "+ COLUMN_GEOCODING + " string not null, " + COLUMN_NB_PHOTO + " integer, "+ COLUMN_TIR_EQUIPE + " integer, "+
    COLUMN_TIR_ADVERSE + " integer, " + COLUMN_NOM_EQUIPE + " string not null, " + COLUMN_PHOTO + " string not null);";


    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(MySQLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GAME);
        onCreate(db);
    }
}
