package threadslifecycle.android.fr.handroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ListeMatches extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_matches);
        DBManager db = new DBManager(this, this);
        db.execute();

    }
}
