package threadslifecycle.android.fr.handroid;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;

public class DetailMatch extends AppCompatActivity {

    int[] dataEq;
    int[] dataAdv;
    PieChart grapheRatioEq;
    PieChart grapheRatioAdv;
    String[] xData = {"Buts","Tirs ratés"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_match);
        Intent intent = getIntent();
        final Game game = (Game)intent.getSerializableExtra("DetailGame");

        TableRow tableName = new TableRow(this);
        TableRow.LayoutParams param = new TableRow.LayoutParams(
                TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT,
                1
        );

        tableName.setLayoutParams(param);
        

        TextView equipe = new TextView(this);
        equipe.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT,1));
        equipe.setText(game.nom_equipe+" ");
        equipe.setTextAppearance(getApplicationContext(),R.style.NomEquipes);
        tableName.addView(equipe);


        TextView adversaire = new TextView(this);
        adversaire.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT,1));
        adversaire.setText(game.adversaire);
        adversaire.setTextAppearance(getApplicationContext(),R.style.NomEquipes);
        tableName.addView(adversaire);



        TableRow tableScore = new TableRow(this);
        tableScore.setLayoutParams(param);

        TextView scoreEquipe = new TextView(this);
        scoreEquipe.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT,1));
        scoreEquipe.setText(Integer.toString(game.score_equipe)+" ");
        scoreEquipe.setTextAppearance(getApplicationContext(),R.style.Score);
        tableScore.addView(scoreEquipe);


        TextView scoreAdversaire = new TextView(this);
        scoreAdversaire.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT,1));
        scoreAdversaire.setText(Integer.toString(game.score_adverse)+" ");
        scoreAdversaire.setTextAppearance(getApplicationContext(),R.style.Score);
        tableScore.addView(scoreAdversaire);



        /*TextView TirEquipe = new TextView(this);
        TirEquipe.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
        TirEquipe.setText(game.tir_equipe+" ");
        table.addView(TirEquipe);

        TextView TirAdverse = new TextView(this);
        TirAdverse.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
        TirAdverse.setText(game.tir_adverse+" ");
        table.addView(TirAdverse);*/

        float ratioEquipe = 0;
        float ratioAdverse = 0;
        //pour les ratios :

            if (game.tir_equipe != 0)ratioEquipe = game.score_equipe*100 / (game.tir_equipe+game.score_equipe);
        if (game.tir_adverse != 0)ratioAdverse = game.score_adverse*100 / (game.tir_adverse+game.score_adverse);

/*        TextView RatioEquipe = new TextView(this);
        RatioEquipe.setText(ratioEquipe+"%");
        table.addView(RatioEquipe);

        TextView RatioAdverse = new TextView(this);
        RatioAdverse.setText(ratioAdverse+"%");
        table.addView(RatioAdverse);*/


        grapheRatioEq = (PieChart) findViewById(R.id.IDgraphe);
        grapheRatioAdv = (PieChart) findViewById(R.id.IDgrapheAdv);

        dataEq = new int[]{game.score_equipe, game.tir_equipe};
        dataAdv = new int[]{game.score_adverse, game.tir_adverse};

        addDataSet(ratioEquipe,dataEq, grapheRatioEq);
        addDataSet(ratioAdverse,dataAdv, grapheRatioAdv);

        TableLayout t = (TableLayout) findViewById(R.id.detail);
        t.addView(tableName);
        t.addView(tableScore);

        if (game.photo!=null && game.photo!=" "){
            ImageView img = findViewById(R.id.photoMatch);
            img.setImageURI(Uri.parse(game.photo));
        }
    }

    //pour afficher les ratios en mode grpahique circulaire
    private void addDataSet(float ratio, int[] yData,PieChart grapheRatio){
        //le conseil du jour : utiliser des log.d pour checker


        ArrayList<PieEntry> yEntrys = new ArrayList<>();
        ArrayList<String> xEntrys = new ArrayList<>();

        for(int i = 0; i < yData.length ; i++)
        {
            yEntrys.add(new PieEntry(yData[i], i));
        }

        PieDataSet dataSet = new PieDataSet(yEntrys, "% de réussite");

        for(int i = 1; i < xData.length ; i++)
        {
            xEntrys.add(xData[i]);
        }

        PieDataSet pieDataSet = new PieDataSet(yEntrys,"Tirs réussis / ratés");
        pieDataSet.setSliceSpace(3); //espace entre les datas
        pieDataSet.setValueTextSize(12); //taille des datas

        ArrayList<Integer> couleurs = new ArrayList<>();
        couleurs.add(Color.GREEN);
        couleurs.add(Color.RED);

        pieDataSet.setColors(couleurs);

        grapheRatio.setCenterText(Float.toString(ratio)+"%"); //affichage du texte au centre
        grapheRatio.setCenterTextSize(13);
        grapheRatio.getDescription().setEnabled(false);
        grapheRatio.getLegend().setEnabled(false);

        //on set les data au graphe
        PieData pieData =  new PieData(pieDataSet);
        grapheRatio.setData(pieData);
        grapheRatio.invalidate();

    }
}
