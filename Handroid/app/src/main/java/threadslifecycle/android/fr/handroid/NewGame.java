package threadslifecycle.android.fr.handroid;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.io.Serializable;

public class NewGame extends AppCompatActivity implements LocationListener, OnMapReadyCallback, GoogleMap.OnMapClickListener{
    private LocationManager locationManager;
    private String provider;
    GoogleMap mMap = null;
    Marker marker = null;
    double lat, lng;
    Location loc = null;
    String lieux = " ";
    Game newGame;
    public static final String PREFNAME1 = "nomEquipe";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_game);
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Get the location manager
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        // Define the criteria how to select the location provider -> use
        // default
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        System.out.println("Provider " + provider + " has been selected.");

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            mMap.setMyLocationEnabled(true);
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            //latituteField.setText("Location not available");
            return;
        }

        Location location = locationManager.getLastKnownLocation(provider);
        lat = location.getLatitude();
        lng = location.getLongitude();
        loc = location;

        // Initialize the location fields
        if (location != null) {
            System.out.println("Provider " + provider + " has been selected.");

        }
        Button valider = (Button) findViewById(R.id.valider);


        valider.setOnClickListener(new View.OnClickListener()      //Creation du listener sur ce bouton
        {
            public void onClick(View actuelView)    //au clic sur le bouton
            {
                newGame = new Game();
                SharedPreferences settings1 = getSharedPreferences(PREFNAME1, 0);
                String nomEquipe = settings1.getString("key1", "Mon Equipe");
                newGame.geocoding = lieux;
                newGame.latitude = lat;
                newGame.longitude = lng;
                newGame.nb_photo = 0;
                newGame.nom_equipe = nomEquipe;
                newGame.score_adverse = 0;
                newGame.score_equipe = 0;
                newGame.tir_adverse = 0;
                newGame.tir_equipe = 0;
                newGame.photo =" ";

                EditText adv = (EditText) findViewById(R.id.adversaire);
                newGame.adversaire = String.valueOf(adv.getText());
                if (newGame.adversaire=="") newGame.adversaire="adversaire";


                Intent intent = new Intent(NewGame.this, GameLive.class);  //Lancer l'activité Stats
                intent.putExtra("newGame", (Serializable) newGame);
                startActivity(intent);    //Afficher la vue
            }
        });


    }

    public void onMapClick(LatLng point) {
        lat = point.latitude;
        lng = point.longitude;
        loc.setLatitude(point.latitude);
        loc.setLongitude(point.longitude);
        lieux = getLocationName(this, loc);
        TextView localisation = (TextView) findViewById(R.id.lieux);
        localisation.setText(lieux);
        if (marker != null) {
            marker.remove();
        }
        marker = mMap.addMarker(new MarkerOptions().position(point)
                .title("position du Match"));
    }




    @Override
    public void onLocationChanged(Location location) { loc = location; }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(this, "Enabled new provider " + provider,
                Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Disabled provider " + provider,
                Toast.LENGTH_SHORT).show();
    }
    public static final String NO_LOCATION_NAME = "NONE";

    public static String getLocationName (Activity activity, Location location){
        String name = NO_LOCATION_NAME;
        if (location != null) {
            Geocoder geocoder = new Geocoder(activity);
            try {
                Address address = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1).get(0);
                name = (address.getSubThoroughfare()+" "+address.getThoroughfare() + ", " +address.getLocality()+", "+ address.getAdminArea() + ", " + address.getCountryName());
                System.out.println("adress "+address);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return name;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        LatLng now = new LatLng(lat, lng);
        loc.setLatitude(lat);
        loc.setLongitude(lng);
        System.out.println("coucou: "+ lat+" "+lng);
        marker = mMap.addMarker(new MarkerOptions().position(now).title("position du Match"));
        lieux = getLocationName(this, loc);
        TextView localisation = (TextView) findViewById(R.id.lieux);
        localisation.setText(lieux);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(now));
        float zoomLevel = 16.0f; //This goes up to 21
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(now, zoomLevel));
        mMap.setOnMapClickListener(this);



    }
}
