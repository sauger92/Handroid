package threadslifecycle.android.fr.handroid;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class GameDataSource {
    // Champs de la base de données
    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = { MySQLiteHelper.COLUMN_ID,
            MySQLiteHelper.COLUMN_ADVERSAIRE,MySQLiteHelper.COLUMN_SCORE_EQUIPE, MySQLiteHelper.COLUMN_SCORE_ADVERSE, MySQLiteHelper.COLUMN_LATITUDE,
            MySQLiteHelper.COLUMN_LONGITUDE, MySQLiteHelper.COLUMN_GEOCODING,MySQLiteHelper.COLUMN_NB_PHOTO,MySQLiteHelper.COLUMN_TIR_EQUIPE,
            MySQLiteHelper.COLUMN_TIR_ADVERSE,MySQLiteHelper.COLUMN_NOM_EQUIPE, MySQLiteHelper.COLUMN_PHOTO};

    public GameDataSource(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() { dbHelper.close(); }

    public Game createGame(Game game) {

        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_ADVERSAIRE, game.adversaire);
        values.put(MySQLiteHelper.COLUMN_SCORE_EQUIPE, game.score_equipe);
        values.put(MySQLiteHelper.COLUMN_SCORE_ADVERSE, game.score_adverse);
        values.put(MySQLiteHelper.COLUMN_LATITUDE, game.latitude);
        values.put(MySQLiteHelper.COLUMN_LONGITUDE,game.longitude);
        values.put(MySQLiteHelper.COLUMN_GEOCODING, game.geocoding);
        values.put(MySQLiteHelper.COLUMN_NB_PHOTO, game.nb_photo);
        values.put(MySQLiteHelper.COLUMN_TIR_EQUIPE,game.tir_equipe);
        values.put(MySQLiteHelper.COLUMN_TIR_ADVERSE, game.tir_adverse);
        values.put(MySQLiteHelper.COLUMN_NOM_EQUIPE, game.nom_equipe);
        values.put(MySQLiteHelper.COLUMN_PHOTO, game.photo);

        long insertId = database.insert(MySQLiteHelper.TABLE_GAME, null,
                values);
        Cursor cursor = database.query(MySQLiteHelper.TABLE_GAME,
                allColumns, MySQLiteHelper.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Game newGame = cursorToComment(cursor);
        cursor.close();
        return newGame;
    }

    public void deleteComment(Game comment) {
        long id = comment.id;
        System.out.println("Comment deleted with id: " + id);
        database.delete(MySQLiteHelper.TABLE_GAME, MySQLiteHelper.COLUMN_ID
                + " = " + id, null);
    }

    public List<Game> getAllComments() {
        List<Game> listgames = new ArrayList<Game>();

        Cursor cursor = database.query(MySQLiteHelper.TABLE_GAME,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Game game = cursorToComment(cursor);
            listgames.add(game);
            cursor.moveToNext();
        }
        // assurez-vous de la fermeture du curseur
        cursor.close();
        return listgames;
    }

    private Game cursorToComment(Cursor cursor) {
        Game game = new Game();
        game.id = cursor.getInt(0);
        game.adversaire = cursor.getString(1);
        game.score_equipe = cursor.getInt(2);
        game.score_adverse = cursor.getInt(3);

        game.nb_photo = cursor.getInt(4);
        game.tir_equipe = cursor.getInt(5);
        game.latitude = cursor.getDouble(6);
        game.longitude = cursor.getDouble(7);
        game.geocoding = cursor.getString(8);

        game.tir_adverse = cursor.getInt(9);
        game.nom_equipe = cursor.getString(10);
        game.photo = cursor.getString(11);

        return game;
    }

}
